import { Dimensions, StyleSheet, Text, TouchableOpacity,View } from "react-native";
import LinearGradient from 'react-native-linear-gradient';



export default ({ onPress, text, size, theme }) => {





  const buttonStyles = [styles.button];
  const textStyles = [styles.text];

  if (size === "double") {
    buttonStyles.push(styles.buttonDouble);
  }

  if (theme === "secondary") {
    buttonStyles.push(styles.buttonSecondary);
    textStyles.push(styles.textSecondary);
  } else if (theme === "accent") {
    buttonStyles.push(styles.buttonAccent);
  }else if (theme === "accent1") {
    buttonStyles.push(styles.buttonAccent1);
  }

  return (
  
      <TouchableOpacity onPress={onPress} style={buttonStyles}>
      
      <Text style={textStyles}>{text}</Text>
    </TouchableOpacity>


 
 
      
   
  );
};

// set dimmenstion
const screen = Dimensions.get("window");
const buttonWidth = screen.width /4;

const styles = StyleSheet.create({
 

  button: {
    backgroundColor: 'rgba(255, 255, 255, 0.2)', // Transparent white background
    borderWidth: 1,
    borderColor: 'rgba(255, 255, 255, 0.4)',
    flex: 1,
    height: Math.floor(buttonWidth - 35),
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 6,
    margin: 5,
    position: 'relative',
   
  },
 
  text: {
    color: "#fff",
    fontSize:18,

  },
  textSecondary: {
    color: "white",
   
    fontWeight:'bold',
    borderRadius:6
  },
  buttonDouble: {
    width: screen.width / 2 - 10,
    flex: 0,
    alignItems: "flex-start",
    paddingLeft: 40,
  },
  buttonSecondary: {
    backgroundColor: "#990F02",
    borderRadius:6
  },
  buttonAccent: {
    backgroundColor: "#FF8C00",
    borderRadius:6
  },
  buttonAccent1: {
    backgroundColor: "#990F02",
    borderRadius:6
  },
});