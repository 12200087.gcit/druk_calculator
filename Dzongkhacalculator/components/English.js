

import React, { Component } from "react";
import { SafeAreaView, StyleSheet, Text, View, TouchableOpacity, ImageBackground, Dimensions ,TextInput} from "react-native";
import Button from "./Button";
import Row from "./Row";
import { library } from '@fortawesome/fontawesome-svg-core';
import { faDeleteLeft } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';



library.add(faDeleteLeft);


const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const screen = Dimensions.get('window');
const scaleFactor = screen.width / 375;

export default class English extends Component {
  state = {
    currentValue: "",
    inputStack: [], // Maintain a stack of user inputs
    history: [],
    isEqualsPressed: false, // Track if the '=' button is pressed
    openBracket: true, // Track if "(" should be displayed
  };

  handleTap = (value) => {
    if (value === "Del") {
      // Pop the last input from the stack and update the current input
      const { inputStack } = this.state;
      if (inputStack.length > 0) {
        const updatedStack = [...inputStack];
        updatedStack.pop();
        const updatedValue = updatedStack.join("");
        this.setState({
          currentValue: updatedValue,
          inputStack: updatedStack,
        });
      } else {
        // Clear both the current value and the history if the input stack is empty
        this.setState({
          currentValue: "",
          inputStack: [],
          history: [],
          isEqualsPressed: false, // Reset the equals pressed flag
        });
      }
    } else if (value === "=") {
      // Join the equation with '=' and update the current input
      const equation = this.state.inputStack.join("");
      if (equation) {
        this.calculate();
      }
    } else {
      // Append the clicked value to the current input and update the stack
      this.setState((prevState) => {
        let newValue = value;
        // Check if the previous character was a closing bracket and the current value is a number
        const lastChar = prevState.currentValue.slice(-1);
        if (lastChar === ")" && !isNaN(parseInt(value))) {
          // Automatically insert "*" before appending the new value
          newValue = "*" + newValue;
        }
        return {
          currentValue: prevState.currentValue + newValue,
          inputStack: [...prevState.inputStack, newValue],
          isEqualsPressed: false, // Reset the equals pressed flag
        };
      });
    }
  };

  handleBracket = () => {
    this.setState((prevState) => ({
      currentValue: prevState.currentValue + (prevState.openBracket ? "(" : ")"),
      inputStack: [...prevState.inputStack, prevState.openBracket ? "(" : ")"],
      openBracket: !prevState.openBracket, // Toggle between "(" and ")"
      isEqualsPressed: false, // Reset the equals pressed flag
    }));
  };

  calculate = () => {
    try {
      let expression = this.state.currentValue;
      // Replace English numerals with Arabic numerals
      expression = expression
        .replace(/1/g, "1")
        .replace(/2/g, "2")
        .replace(/3/g, "3")
        .replace(/4/g, "4")
        .replace(/5/g, "5")
        .replace(/6/g, "6")
        .replace(/7/g, "7")
        .replace(/8/g, "8")
        .replace(/9/g, "9")
        .replace(/0/g, "0")
        .replace(/00/g, "00");
  
      // Handle both types of expressions (with and without brackets)
      expression = expression
        .replace(/(\d+)\((\d+)\)/g, "$1*$2") // Replace (༡+༢) with (1+2)
        .replace(/(\d+)\(/g, "$1*(")        // Insert a multiplication operator before (
        .replace(/\)(\d+)/g, ")*$1");       // Replace )3 with )*3
  
      const result = eval(expression);
  
      if (isNaN(result)) {
        // Handle the case when the result is NaN
        return "Error";
      }
      const formattedResult = Number(result).toFixed(8).replace(/\.?0+$/, '');
  
      const resultInEnglishNumerals = formattedResult
        .toString()
        .replace(/1/g, "1")
        .replace(/2/g, "2")
        .replace(/3/g, "3")
        .replace(/4/g, "4")
        .replace(/5/g, "5")
        .replace(/6/g, "6")
        .replace(/7/g, "7")
        .replace(/8/g, "8")
        .replace(/9/g, "9")
        .replace(/0/g, "0")
        .replace(/00/g, "00");
  
      this.setState((prevState) => ({
        history: [...prevState.history, { expression: expression, formattedResult}],
        currentValue: resultInEnglishNumerals,
      }));
  
      return resultInEnglishNumerals;
    } catch (error) {
      return "Error";
    }
  };
  

  render() {
    const fontSize = 42 * scaleFactor; 
    return (
   
      <View style={styles.container}>
  
        <SafeAreaView>
        
        <View>
        
        <ImageBackground  style={[styles.fixedView]} source={require('../assets/22.png')}>

        
        <View style={[styles.value,{fontSize}]}>

            <TextInput
            value={this.state.inputStack.join("")}

            showSoftInputOnFocus={false}
            style={styles.input}/>

          </View>  


        <TextInput
        value={this.state.currentValue}

        showSoftInputOnFocus={false}
        style={styles.input1}/>
      
        </ImageBackground>

    
      
        </View>

       
          
             
          <Row>
            <Button
              theme="accent1"
              text="C"
              onPress={() =>
                this.setState({ currentValue: "", inputStack: [] })
              }
            />

            <TouchableOpacity
              theme="primary"
              onPress={() => this.handleTap("Del")}
              style={{
                backgroundColor: 'rgba(255, 255, 255, 0.2)', // Transparent white background
                borderWidth: 1,
                borderColor: 'rgba(255, 255, 255, 0.4)',
                flex: 1,
                alignItems: "center",
                justifyContent: "center",
                borderRadius: 8,
                margin: 5,
              }}
            >
              <View
              style={{
                width: '100%',
                height: '52%', // 70% of the button's height to create a semi-circle
            
                borderRadius: 100, // Half the height to make a semi-circle
                borderBottomLeftRadius:0,
                borderBottomRightRadius:0,
                position: 'absolute',
                top:25,
                
              }}>

              </View>
              <FontAwesomeIcon icon={faDeleteLeft} size={30} color="white" />
            </TouchableOpacity>
            <Button
              text={this.state.openBracket ? "(   )" : ")"}
              onPress={this.handleBracket}
            />
          
            <Button theme="accent"  text="+" onPress={() => this.handleTap("+")} />
          </Row>
          <Row>
            <Button text="7" onPress={() => this.handleTap("7")} />
            <Button text="8" onPress={() => this.handleTap("8")} />
            <Button text="9" onPress={() => this.handleTap("9")} />
            <Button theme="accent" text="-" onPress={() => this.handleTap("-")} />
         
          </Row>
          <Row>
            <Button text="4" onPress={() => this.handleTap("4")} />
            <Button text="5" onPress={() => this.handleTap("5")} />
            <Button text="6" onPress={() => this.handleTap("6")} />
           
            <Button theme="accent" text="*" onPress={() => this.handleTap("*")} />
          </Row>
          <Row>
            <Button text="1" onPress={() => this.handleTap("1")} />
            <Button text="2" onPress={() => this.handleTap("2")} />
            <Button text="3" onPress={() => this.handleTap("3")} />
       
            <Button theme="accent" text="/"  onPress={() => this.handleTap("/")} />
          </Row>
          <Row>
            <Button text="0" onPress={() => this.handleTap("0")} />
            <Button text="00" onPress={() => this.handleTap("00")} />
            <Button text="." onPress={() => this.handleTap(".")} />
            
              <Button
              text="="
              theme="accent"
            
      
             
              onPress={() => {
                if (this.state.currentValue !== "Error") {
                  this.setState({ currentValue: this.calculate() });
                }
              }}
            />
          </Row>
        </SafeAreaView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent:"flex-end",
  },
  value: {
    color: "#FF8C00",
    fontSize:18,
  

 
  
  },
  historyContainer: {
    marginTop:205,// Adjust the border color as needed
    padding: 10,
  
  },
  historyText: {
    color: "white",
    fontSize: 18,
    textAlign: "right",
    marginRight: 20,
  },
  backgroundImage: {
    justifyContent:'center',
    alignContent:'center',
    
  },
  tvScreen: {
    borderWidth:1, // Border width for the TV screen
    borderColor: 'green', // Border color for the TV screen
    borderRadius:6, // Adjust the border radius to make it look like a TV screen
    margin:5,
    height:290


  },

  fixedView: {
    justifyContent:'flex-end',
    alignItems:'flex-end',
    margin:0,
    width: windowWidth * 1, // Adjust as needed
    height: windowHeight * 0.5, // Adjust as needed
    resizeMode:'cover', // or 'cover' depending on your requirements
 
  },
  text: {
    color: 'white',
    fontFamily:"sans-serif",
    marginRight: 14,
    fontSize:18,
    textAlign: 'right',
    
  },
  input: {

    fontSize: windowWidth * 0.09,
    textAlign: "right",
    color:'white',
    marginRight: windowWidth * 0.02,
    fontWeight: "300",
    fontFamily:"sans-serif",
    width:windowWidth*0.96,
  

   
  },
  input1: {
    fontSize: windowWidth * 0.1,
   
    textAlign: "right",
    color: "white",
    marginRight: windowWidth * 0.02,
    fontWeight:"300",
    fontFamily:"sans-serif",
    width:windowWidth*0.97,
    

   
  },
 
 
});
