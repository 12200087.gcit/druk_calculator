import React, { Component } from "react";
import { StyleSheet, View, Text, Switch, StatusBar, Alert, BackHandler, Platform, TouchableOpacity, Modal, Dimensions,Image, ScrollView} from "react-native";
import Dzongkha from "./Dzongkha";
import English from "./English";
import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faEllipsisVertical } from "@fortawesome/free-solid-svg-icons";
import { faRectangleXmark } from "@fortawesome/free-solid-svg-icons";
import * as Font from 'expo-font';


library.add(faRectangleXmark);

const screen = Dimensions.get('window');
const scaleFactor = screen.width / 375;
const customFont = require('../assets/DDC_Uchen_Numbers.ttf');

const loadCustomFont = async () => {
    await Font.loadAsync({
      'DDC_Uchen_Numbers': customFont,
    });
  };
  
  // Call the function to load the font
  loadCustomFont();

export default class Main extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentPage: "dzongkha",
      isChecked: false,
      isModalVisible: false,
    };
  }

  togglePage = (page) => {
    this.setState({ currentPage: page });
  };

  toggleSwitch = () => {
    this.setState((prevState) => ({
      isChecked: !prevState.isChecked,
    }));
    const nextPage = this.state.currentPage === "English" ? "dzongkha" : "English";
    this.togglePage(nextPage);
  };

  toggleModal = () => {
    this.setState((prevState) => ({
      isModalVisible: !prevState.isModalVisible,
    }));
  };

  handleBackButton = () => {
    Alert.alert(
      "Exit App",
      "Are you sure you want to exit the app?",
      [
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel",
        },
        {
          text: "Exit",
          onPress: () => {
            if (Platform.OS === "android") {
              BackHandler.exitApp();
            } else {
              // Minimize the app on iOS
            }
          },
        },
      ],
      { cancelable: false }
    );
    return true;
  };

  componentDidMount() {
    if (Platform.OS === "android") {
      BackHandler.addEventListener("hardwareBackPress", this.handleBackButton);
    }
  }

  componentWillUnmount() {
    if (Platform.OS === "android") {
      BackHandler.removeEventListener(
        "hardwareBackPress",
        this.handleBackButton
      );
    }
  }

  render() {
    const fontSize = 20 * scaleFactor;
    const { isChecked, isModalVisible } = this.state;
    let pageContent;

    if (this.state.currentPage === "dzongkha") {
      pageContent = <Dzongkha />;
    } else if (this.state.currentPage === "English") {
      pageContent = <English />;
    }

    const modalContent = isChecked ? (
      <ScrollView>
      <View style={styles.container1}>
       
        <View>
          <View style={{ flex: 1, justifyContent: 'center', alignItems: 'left',height:50}}>
          <Text style={{color:"black",fontWeight:"bold",fontSize:16}}>About App</Text>
    

          </View>
        
        <Text style={{marginTop:5,fontSize:14}}>
          "Our mission: Simplifying math in Dzongkha for everyone." Providing a calculator in Dzongkha can make mathematical computations more accessible to a wider population, including those who are not proficient in English. We understand the importance of preserving and promoting Dzongkha, and our app is a small step toward that goal.
        </Text>
    

        <View style={{marginTop:5, flex: 1, justifyContent: 'center', alignItems: 'left',height:50}}>
          <Text style={{color:"black",fontWeight:"bold",fontSize:16}}>Future Plan</Text>
    

          </View>
        <Text style={{fontSize:14,marginTop:5}}>
          1. Input voice and output.{'\n'}
          2. To include Advanced Math Functions.{'\n'}
          3. Customization Options for users.{'\n'}
          4. Actively gather user feedback to identify areas for improvement.
        </Text>

        </View>
     
        <View style={{marginTop:5, flex: 1, justifyContent: 'center', alignItems: 'left',height:50}}>
          <Text style={{color:"black",fontWeight:"bold",fontSize:16}}>Developer</Text>
    

          </View>
        
        <Text style={{marginTop:5,fontSize:14}}>
          Pema Norbu{'\n'}
          Tandin Jamtsho{'\n'}
          Gyem Tshering
        </Text>
        
      
 	

        
        <View style={{  flex: 1, justifyContent: 'center', alignItems: 'left',height:50}}>
          <Text style={{color:"black",fontWeight:"bold",fontSize:16,marginTop:10}}>Dzongkha Support</Text>
        </View>
        
        <Text style={{marginTop:5,fontSize:14}}>
          Namgay Thinley{'\n'}
          Singye Dorji
        </Text>
        <View style={{  flex: 1, justifyContent: 'center', alignItems: 'left',height:50}}>
          <Text style={{color:"black",fontWeight:"bold",fontSize:16,marginTop:10}}>In Collaboration with DCDD</Text>
        </View>
  
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
          <Image
            source={require('../assets/unnamed_1_1-removebg-preview.png')}
            style={{ width: 120, height:120}}
          />
        </View>
      </View>
      </ScrollView>
    ) : (
      <ScrollView>
      <View style={styles.container1}>
      
        <View>
        <View style={{  flex: 1, justifyContent: 'center', alignItems: 'left',height:50}}>
          <Text style={{color:"black",fontWeight:"bold",fontSize:20,fontFamily:"DDC_Uchen_Numbers"}}>རིམ་ལུགས་ཀྱི་སྐོར།</Text>
    

          </View>
        <Text style={{marginTop:5,fontSize:16,fontFamily:"DDC_Uchen_Numbers"}}>
        “དམིགས་བསྐྱེད། ག་ར་ལུ་རྫོང་ཁའི་ནང་རྩིས་རྐྱབ་ནི་འཇམ་ཏོང་ཏོ་བཟོ་ནི།” རྫོང་ཁའི་ནང་རྩིས་འཕྲུལ་ལུ་བརྟེན་ རྩིས་རྐྱབ་ནིའི་གོ་སྐབས་འདི་ མི་མང་དང་ལྷག་པར་དུ་ཨིང་སྐད་མ་ཤེས་མི་ཚུ་ལུ་བྱིན་ཚུགས། རྫོང་ཁ་ཉམས་སྲུང་དང་གོང་འཕེལ་གཏང་ནི་གི་དམིགས་ཡུལ་ལུ་ རིམ་ལུགས་དེ་གིས་དུམ་གྲ་ཅིག་ཨིན་རུང་ཕན་ཐོགས་པའི་རེ་བ་ཡོད།
        </Text>
     

        <View style={{  flex: 1, justifyContent: 'center', alignItems: 'left',height:50}}>
          <Text style={{color:"black",fontWeight:"bold",fontSize:20,fontFamily:"DDC_Uchen_Numbers"}}>མ་འོངས་ལས་འཆར།</Text>
    

          </View>
        <Text style={{marginTop:5,fontSize:16,fontFamily:"DDC_Uchen_Numbers"}}>
        ༡༽  ནང་བཙུགས་དང་ཕྱིར་ཐོན་སྒ།{'\n'}
        ༢༽  ཆེ་རིམ་རྩིས་ལས་ཞབས་ཏོག་བཙུགས་ནི།{'\n'}
        ༣༽  ལག་ལེན་པ་ལུ་ལས་རིགས་གདམ་ཁ།{'\n'}
        ༤༽  ལེགས་བཅོས་དོན་ལུ་ལག་ལེན་པའི་བསམ་ལན་བསྡུ་ནི།

      

        </Text>

        </View>
      
        <View style={{  flex: 1, justifyContent: 'center', alignItems: 'left',height:50}}>
          <Text style={{color:"black",fontWeight:"bold",fontSize:20,fontFamily:"DDC_Uchen_Numbers"}}>གསར་བཟོ།</Text>
    

          </View>
        <Text style={{marginTop:5,fontSize:16,fontFamily:"DDC_Uchen_Numbers"}}>
          པདྨ་ནོར་བུ།{'\n'}
          རྟ་མགྲིན་རྒྱ་མཚོ།{'\n'}
          མགོནམ་ཚེ་རིང།

        </Text>
	

       
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'left',height:50}}>
          <Text style={{color:"black",fontWeight:"bold",fontSize:20,fontFamily:"DDC_Uchen_Numbers"}}>རྫོང་ཁའི་རྒྱབ་སྐྱོར།</Text>
    

          </View>
        <Text style={{marginTop:5,fontSize:16,fontFamily:"DDC_Uchen_Numbers"}}>
        རྣམ་རྒྱལ་འཕྲིན་ལས། {'\n'}
        སེང་གེ་རྡོ་རྗེ།	{'\n'}

        </Text>

        <Text style={{fontSize:16,marginTop:5,fontFamily:"DDC_Uchen_Numbers"}}>རིམ་ལུག་འདི་སྲོལ་འཛིན་དང་རྫོང་ཁ་གོང་འཕེལ་ལས་ཁུངས།དང་མཉམ་འབྲེལ་ཐོག་ལས་བཟོ་བཟོཝ་ཨིན།</Text>
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
          <Image
            source={require('../assets/unnamed_1_1-removebg-preview.png')}
            style={{ width: 120, height:120,marginTop:2}}
          />
        </View>
      </View>
      </ScrollView>
    );

    return (
      <View style={styles.container}>

        {/* <StatusBar
          animated={true}
          backgroundColor="#FF8C00"
          barStyle="light-content"
          showHideTransition="fade"
          hidden={false}
        /> */}

        <View style={styles.switchContainer}>

          <Switch
            style={{ marginLeft: 10 }}
            trackColor={{ false: "#ccc", true: "white" }}
            thumbColor={isChecked ? "#fff" : "#FF8C00"}
            ios_backgroundColor="#ccc"
            onValueChange={this.toggleSwitch}
            value={isChecked}
          />
          <Text style={[styles.text, { fontSize }]}>
            {isChecked ? "English" : "རྫོང་ཁ།"}
          </Text>
          <View style={{ flex: 1, flexDirection: "row", justifyContent: "flex-end", marginRight: 15 }}>
            <TouchableOpacity onPress={this.toggleModal}>
              <FontAwesomeIcon
                icon={faEllipsisVertical}
                size={24}
                style={{ color: "#ffffff" }}
              />
            </TouchableOpacity>
          </View>
        </View>

        {pageContent}

        <Modal
          visible={isModalVisible}
          animationType="slide"
          transparent={true}
          onRequestClose={this.toggleModal}
        >
          <View style={styles.modalContainer}>
            <TouchableOpacity
              style={styles.closeButton}
              onPress={this.toggleModal}
            >
              <FontAwesomeIcon
                icon={faRectangleXmark}
                size={24}
                style={{ color: "red" }}
              />
            </TouchableOpacity>
            {modalContent}
          </View>
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "black",
    justifyContent: "flex-end",
  },
  switchContainer: {
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
    backgroundColor: "transparent",
    height: 75,
    zIndex: 1,
  },
  text: {
    color: 'white',
    fontFamily:"DDC_Uchen_Numbers",
    marginRight: 14,
    fontSize: 18,
    textAlign: 'right',
  },
  container1: {
    flex: 1,
    padding: 16,
    top:0
  },
  heading: {
    fontSize: 14,
    color: 'black',
    fontFamily:"DDC_Uchen_Numbers",
    letterSpacing: 1,
    marginTop: 60,
  },
  heading5: {
    fontSize:16,
    color: 'black',
    fontFamily:"DDC_Uchen_Numbers",
    letterSpacing: 1,
    marginTop:35,
    fontWeight:'bold'
  },
  heading1: {
    fontSize: 14,
    color: 'black',
    fontFamily:"DDC_Uchen_Numbers",
    letterSpacing: 1,
    marginTop: 15,
  },
  heading2: {
    fontSize: 14,
    color: 'black',
    fontFamily:"DDC_Uchen_Numbers",
    letterSpacing: 1,
    marginTop: 15,
  },
  heading3: {
    fontSize:16,
    color: 'black',
    fontFamily:"DDC_Uchen_Numbers",
    letterSpacing: 1,
    marginTop: 15,
    fontWeight:'bold'
  },
  aboutText: {
    fontSize: 11,
    color: 'black',
    fontFamily:"DDC_Uchen_Numbers",
    letterSpacing: 1,
    marginTop: 3,
  },
  aboutText1: {
    fontSize: 13,
    color: 'black',
    fontFamily:"DDC_Uchen_Numbers",
    letterSpacing: 1,
    marginTop: 3,
  },
  futurePlanText: {
    fontSize: 11,
    color: 'black',
    fontFamily:"DDC_Uchen_Numbers",
    letterSpacing: 1,
    marginTop: 3,
  },
  developerText: {
    fontSize: 11,
    color: 'black',
    fontFamily:"DDC_Uchen_Numbers",
    marginTop: 3,
  },
  modalContainer: {
    flex: 1,
    backgroundColor: "white", // Change the background color as needed
    
  },
  closeButton: {
    position: "absolute",
    top: 10,
    right: 10,
    zIndex:1
  },
});
