

import React, { Component } from "react";
import { SafeAreaView, StyleSheet, Text, View, TouchableOpacity, ImageBackground, Dimensions ,TextInput,PixelRatio} from "react-native";
import Button from "./Button";
import Row from "./Row";
import { library } from '@fortawesome/fontawesome-svg-core';
import { faDeleteLeft } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import Custombutton from "./Custombutton";


library.add(faDeleteLeft);


const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;
const screen = Dimensions.get('window');
const scaleFactor = screen.width / 375;
// set dimmenstion
const buttonWidth = screen.width /4;
const fontSize = (size) => {
  const scale = screenWidth / 320; // You can adjust the base size (e.g., 320) as needed
  const newSize = size * scale;
  return Math.round(PixelRatio.roundToNearestPixel(newSize));
};

export default class Dzongkha extends Component {
  state = {
    currentValue: "",
    inputStack: [], // Maintain a stack of user inputs
    history: [],
    isEqualsPressed: false, // Track if the '=' button is pressed
    openBracket: true, // Track if "(" should be displayed
  };

 handleTap = (inputValue) => {
   if (inputValue === "Del") {
     this.handleDelete();
   } else if (inputValue === "=") {
     this.calculate();
   } else {
     this.appendValue(inputValue);
   }
 };

 handleDelete() {
   const { inputStack } = this.state;
   if (inputStack.length > 0) {
     const updatedStack = [...inputStack];
     updatedStack.pop();
     const updatedValue = updatedStack.join("");
     this.setState({
       currentValue: updatedValue,
       inputStack: updatedStack,
     });
   } else {
     // Handle additional cases where the user wants to delete specific characters
     const { inputStack } = this.state;
     if (inputStack.length > 0 && typeof value === "string") {
       const updatedStack = [...inputStack];
       const index = updatedStack.lastIndexOf(value);
       if (index !== -1) {
         updatedStack.splice(index, 1);
         const updatedValue = updatedStack.join("");
         this.setState({
           currentValue: updatedValue,
           inputStack: updatedStack,
         });
       }
     }
   }
 }

 appendValue(inputValue) {
   this.setState((prevState) => {
     let newValue = inputValue;
     const lastChar = prevState.currentValue.slice(-1);
     if (lastChar === ")" && !isNaN(parseInt(inputValue))) {
       newValue = "*" + newValue;
     }
     return {
       currentValue: prevState.currentValue + newValue,
       inputStack: [...prevState.inputStack, newValue],
       isEqualsPressed: false,
     };
   });
 }


  handleBracket = () => {
    this.setState((prevState) => ({
      currentValue: prevState.currentValue + (prevState.openBracket ? "(" : ")"),
      inputStack: [...prevState.inputStack, prevState.openBracket ? "(" : ")"],
      openBracket: !prevState.openBracket, // Toggle between "(" and ")"
      isEqualsPressed: false, // Reset the equals pressed flag
    }));
  };

  calculate = () => {
    try {
      let expression = this.state.currentValue;
      
      // Define a mapping for Dzongkha to Arabic numerals
      const DzongkhaToArabic = {
        "༡": "1",
        "༢": "2",
        "༣": "3",
        "༤": "4",
        "༥": "5",
        "༦": "6",
        "༧": "7",
        "༨": "8",
        "༩": "9",
        "༠": "0",
        "༠༠": "00",
      };
  
      // Replace Tibetan numerals with Arabic numerals using a global regex
      expression = expression.replace(/[༡-༩༠༠]/g, match => DzongkhaToArabic[match] || match);
  
      // Handle expressions with parentheses
      expression = expression.replace(/(\d+)\(/g, "$1*("); // Add * before (3 to make it 3*(3
      expression = expression.replace(/\)(\d+)/g, ")*$1"); // Replace )3 with )*3
  
      const result = eval(expression);
  
      if (isNaN(result)) {
        // Handle the case when the result is NaN
        return "ནོར་བ།";
      }
      const formattedResult = Number(result).toFixed(8).replace(/\.?0+$/, '');
  
      // Convert the numeric result back to Tibetan numerals
      const resultInDzongkhaNumerals = formattedResult
        .toString()
        .replace(/1/g, "༡")
        .replace(/2/g, "༢")
        .replace(/3/g, "༣")
        .replace(/4/g, "༤")
        .replace(/5/g, "༥")
        .replace(/6/g, "༦")
        .replace(/7/g, "༧")
        .replace(/8/g, "༨")
        .replace(/9/g, "༩")
        .replace(/0/g, "༠")
        .replace(/00/g, "༠༠");
  
      this.setState((prevState) => ({
        history: [...prevState.history, { expression: expression, formattedResult }],
        currentValue: resultInDzongkhaNumerals,
      }));
  
      return resultInDzongkhaNumerals;
    } catch (error) {
      return "ནོར་བ།";
    }
  };
  
  render() {
    const fontSize = 42 * scaleFactor; 
    return (
   
      <View style={styles.container}>
        <View>
        
        <ImageBackground  style={styles.fixedView} source={require('../assets/22.png')} >
        <View style={[styles.value,{fontSize}]}>

          <TextInput
          value={this.state.inputStack.join("")}

          showSoftInputOnFocus={false}
          style={styles.input}/>

          </View>  


          <TextInput
          value={this.state.currentValue}

          showSoftInputOnFocus={false}
          style={styles.input1}/>

        
         
      
        </ImageBackground>
        </View>
        
       
  
        <SafeAreaView>
        
     

       
          
             
          <Row>
            <Button
              theme="accent1"
              text="གསད།"
              onPress={() =>
                this.setState({ currentValue: "", inputStack: [] })
              }
            />

            <TouchableOpacity
              theme="primary"
              onPress={() => this.handleTap("Del")}
              style={{
                backgroundColor: 'rgba(255, 255, 255, 0.2)', // Transparent white background
                borderWidth: 1,
                borderColor: 'rgba(255, 255, 255, 0.4)',
                flex: 1,
                alignItems: "center",
                justifyContent: "center",
                borderRadius: 8,
                margin: 5,
              }}
            >
              <View
              style={{
                width: '100%',
                height: '52%', // 70% of the button's height to create a semi-circle
              
                borderRadius: 100, // Half the height to make a semi-circle
                borderBottomLeftRadius:0,
                borderBottomRightRadius:0,
                position: 'absolute',
                top:25,
                
              }}>

              </View>
              {/* <Text style={{
                color:"white",
                fontSize:18,
              }} >
                གསད།
              </Text> */}
              <FontAwesomeIcon icon={faDeleteLeft} size={30} color="white" />

            </TouchableOpacity>
            <Custombutton
              text={this.state.openBracket ? "(  )" : ")"}
              onPress={this.handleBracket}
            />
          
            {/* <Button theme="accent"  text="བསྡོམས།<br>(+)" onPress={() => this.handleTap("+")} />  */}
            <TouchableOpacity  onPress={() => this.handleTap("+")}>
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: "#FF8C00",height: Math.floor(buttonWidth - 35),
           borderRadius: 6,
           margin: 5,   borderWidth: 1,
           borderColor: 'rgba(255, 255, 255, 0.4)',width: screen.width /4.3 - 5,}}>
              <Text style={{ fontSize: 16, color: 'white'}}>བསྡོམས།</Text>
              <Text style={{ fontSize: 17, marginBottom:1, color: 'white'}}>( + )</Text>
              {/* <Button theme="accent" text="Press me" onPress={() => this.handleTap("+")} /> */}
            </View>

            </TouchableOpacity>
          
            
          </Row>
          <Row>
          <Custombutton text="༧" onPress={() => this.handleTap("༧")} />
            <Custombutton text="༨" onPress={() => this.handleTap("༨")} />
            <Custombutton text="༩" onPress={() => this.handleTap("༩")} />
      
         
            {/* <Button theme="accent" text="ཕབ། (-)" onPress={() => this.handleTap("-")} /> */}

            <TouchableOpacity  onPress={() => this.handleTap("-")}>
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: "#FF8C00",height: Math.floor(buttonWidth - 35),
           borderRadius: 6,
           margin: 5,   borderWidth: 1,
           borderColor: 'rgba(255, 255, 255, 0.4)',width: screen.width /4.3 - 5,}}>
              <Text style={{ fontSize: 16, color: 'white' }}>ཕབ།</Text>
              <Text style={{ fontSize: 17, marginBottom:1, color: 'white'}}>( - )</Text>
              {/* <Button theme="accent" text="Press me" onPress={() => this.handleTap("+")} /> */}
            </View>

            </TouchableOpacity>
          </Row>
          <Row>
            <Custombutton text="༤" onPress={() => this.handleTap("༤")} />
            <Custombutton text="༥" onPress={() => this.handleTap("༥")} />
            <Custombutton text="༦" onPress={() => this.handleTap("༦")} />
            {/* <Button theme="accent" text=" སྒྱུར། (*)" onPress={() => this.handleTap("*")} /> */}

            <TouchableOpacity  onPress={() => this.handleTap("*")}>
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: "#FF8C00",height: Math.floor(buttonWidth - 35),
           borderRadius: 6,
           margin: 5,   borderWidth: 1,
           borderColor: 'rgba(255, 255, 255, 0.4)',width: screen.width /4.3 - 5,}}>
              <Text style={{ fontSize: 16, color: 'white'}}>སྒྱུར།</Text>
              <Text style={{ fontSize: 17, marginBottom:1, color: 'white' }}>( * )</Text>
              {/* <Button theme="accent" text="Press me" onPress={() => this.handleTap("+")} /> */}
            </View>

            </TouchableOpacity>
           
          </Row>
          <Row>
          <Custombutton text="༡" onPress={() => this.handleTap("༡")} />
            <Custombutton text="༢" onPress={() => this.handleTap("༢")} />
            <Custombutton text="༣" onPress={() => this.handleTap("༣")} />
            {/* <Button style={{
              fontSize:16,
              color:"red"
            }} theme="accent" text="བགོ། (/)"  onPress={() => this.handleTap("/")} /> */}


          <TouchableOpacity  onPress={() => this.handleTap("/")}>
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: "#FF8C00",height: Math.floor(buttonWidth - 35),
           borderRadius: 6,
           margin: 5,   borderWidth: 1,
           borderColor: 'rgba(255, 255, 255, 0.4)',width: screen.width /4.3 - 5,}}>
              <Text style={{ fontSize: 16, color: 'white'}}>བགོ།</Text>
              <Text style={{ fontSize: 17, marginBottom:1, color: 'white' }}>( / )</Text>
              {/* <Button theme="accent" text="Press me" onPress={() => this.handleTap("+")} /> */}
            </View>

            </TouchableOpacity>
          </Row>
          <Row>
            <Custombutton text="༠" onPress={() => this.handleTap("༠")} />
            <Custombutton text="༠༠" onPress={() => this.handleTap("༠༠")} />
            <Custombutton text="." onPress={() => this.handleTap(".")} />
            
              {/* <Button
              text="ལན། (=)"
              theme="accent"
            
      
             
              onPress={() => {
                if (this.state.currentValue !== "Error") {
                  this.setState({ currentValue: this.calculate() });
                }
              }}
            /> */}

            <TouchableOpacity    onPress={() => {
                if (this.state.currentValue !== "Error") {
                  this.setState({ currentValue: this.calculate() });
                }
              }}>
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: "#FF8C00",height: Math.floor(buttonWidth - 35),
           borderRadius: 6,
           margin: 5,   borderWidth: 1,
           borderColor: 'rgba(255, 255, 255, 0.4)',width: screen.width /4.3 - 5,}}>
              <Text style={{ fontSize: 16, color: 'white',fontFamily:"DDC_Uchen_Numbers"}}>ལན།</Text>
              <Text style={{ fontSize: 17, marginBottom:1, color: 'white' }}>( = )</Text>
              {/* <Button theme="accent" text="Press me" onPress={() => this.handleTap("+")} /> */}
            </View>

            </TouchableOpacity>
          </Row>
        </SafeAreaView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent:"flex-end",
  },



  fixedView: {
    margin:0,
    borderRadius:6,
    width: windowWidth * 1, // Adjust as needed
    justifyContent:'flex-end',
    height: windowHeight * 0.5, // Adjust as needed
    resizeMode: 'contain', // or 'cover' depending on your requirements
    
  },
  text: {
    color: 'white',
    fontFamily:"DDC_Uchen_Numbers",
    marginRight: 14,
    fontSize:24,
    textAlign: 'right',
   
  },
  input: {

    fontSize: windowWidth * 0.1,
    textAlign: "right",
    color: "white",
    marginRight: windowWidth * 0.02,
    fontWeight: "300",
    fontFamily:"DDC_Uchen_Numbers",
    width:windowWidth*0.96,
   
   
   
  },
  input1: {
    fontSize: windowWidth * 0.15,
    textAlign: "right",
    color: "white",
    marginRight: windowWidth * 0.02,
    fontWeight: "300",
    fontFamily:"DDC_Uchen_Numbers",
    width:windowWidth*0.96,
  

   
  },
  

  
 
 
});
