import { StatusBar } from 'expo-status-bar';
import { StyleSheet,View } from 'react-native';
import Main from './components/Main';

export default function App() {
  return (
    <View style={styles.container}>
 
        <StatusBar barStyle="light" backgroundColor='#FF8C00' />
   
      <Main/>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop:28,
    backgroundColor: 'black',
    alignItems: 'center',
    justifyContent: 'center',
  },

});
